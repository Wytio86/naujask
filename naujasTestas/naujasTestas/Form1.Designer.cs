﻿namespace naujasTestas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.mygtukas7 = new System.Windows.Forms.Button();
            this.mygtukas9 = new System.Windows.Forms.Button();
            this.mygtukas6 = new System.Windows.Forms.Button();
            this.mygtukas3 = new System.Windows.Forms.Button();
            this.mygtukas2 = new System.Windows.Forms.Button();
            this.mygtukas1 = new System.Windows.Forms.Button();
            this.mygtukas5 = new System.Windows.Forms.Button();
            this.mygtukas4 = new System.Windows.Forms.Button();
            this.mygtukas8 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tarpinis = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ekranas.Location = new System.Drawing.Point(13, 12);
            this.Ekranas.Multiline = true;
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(201, 76);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.Text = "0";
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // mygtukas7
            // 
            this.mygtukas7.Location = new System.Drawing.Point(12, 93);
            this.mygtukas7.Name = "mygtukas7";
            this.mygtukas7.Size = new System.Drawing.Size(36, 36);
            this.mygtukas7.TabIndex = 1;
            this.mygtukas7.Text = "7";
            this.mygtukas7.UseVisualStyleBackColor = true;
            this.mygtukas7.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas9
            // 
            this.mygtukas9.Location = new System.Drawing.Point(96, 94);
            this.mygtukas9.Name = "mygtukas9";
            this.mygtukas9.Size = new System.Drawing.Size(36, 36);
            this.mygtukas9.TabIndex = 2;
            this.mygtukas9.Text = "9";
            this.mygtukas9.UseVisualStyleBackColor = true;
            this.mygtukas9.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas6
            // 
            this.mygtukas6.Location = new System.Drawing.Point(96, 136);
            this.mygtukas6.Name = "mygtukas6";
            this.mygtukas6.Size = new System.Drawing.Size(36, 36);
            this.mygtukas6.TabIndex = 3;
            this.mygtukas6.Text = "6";
            this.mygtukas6.UseVisualStyleBackColor = true;
            this.mygtukas6.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas3
            // 
            this.mygtukas3.Location = new System.Drawing.Point(96, 177);
            this.mygtukas3.Name = "mygtukas3";
            this.mygtukas3.Size = new System.Drawing.Size(36, 36);
            this.mygtukas3.TabIndex = 4;
            this.mygtukas3.Text = "3";
            this.mygtukas3.UseVisualStyleBackColor = true;
            this.mygtukas3.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas2
            // 
            this.mygtukas2.Location = new System.Drawing.Point(54, 178);
            this.mygtukas2.Name = "mygtukas2";
            this.mygtukas2.Size = new System.Drawing.Size(36, 36);
            this.mygtukas2.TabIndex = 5;
            this.mygtukas2.Text = "2";
            this.mygtukas2.UseVisualStyleBackColor = true;
            this.mygtukas2.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas1
            // 
            this.mygtukas1.Location = new System.Drawing.Point(12, 177);
            this.mygtukas1.Name = "mygtukas1";
            this.mygtukas1.Size = new System.Drawing.Size(36, 36);
            this.mygtukas1.TabIndex = 6;
            this.mygtukas1.Text = "1";
            this.mygtukas1.UseVisualStyleBackColor = true;
            this.mygtukas1.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas5
            // 
            this.mygtukas5.Location = new System.Drawing.Point(54, 136);
            this.mygtukas5.Name = "mygtukas5";
            this.mygtukas5.Size = new System.Drawing.Size(36, 36);
            this.mygtukas5.TabIndex = 7;
            this.mygtukas5.Text = "5";
            this.mygtukas5.UseVisualStyleBackColor = true;
            this.mygtukas5.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas4
            // 
            this.mygtukas4.Location = new System.Drawing.Point(12, 135);
            this.mygtukas4.Name = "mygtukas4";
            this.mygtukas4.Size = new System.Drawing.Size(36, 36);
            this.mygtukas4.TabIndex = 8;
            this.mygtukas4.Text = "4";
            this.mygtukas4.UseVisualStyleBackColor = true;
            this.mygtukas4.Click += new System.EventHandler(this.mygtukas0);
            // 
            // mygtukas8
            // 
            this.mygtukas8.Location = new System.Drawing.Point(54, 94);
            this.mygtukas8.Name = "mygtukas8";
            this.mygtukas8.Size = new System.Drawing.Size(36, 36);
            this.mygtukas8.TabIndex = 9;
            this.mygtukas8.Text = "8";
            this.mygtukas8.UseVisualStyleBackColor = true;
            this.mygtukas8.Click += new System.EventHandler(this.mygtukas0);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(12, 219);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(78, 36);
            this.button10.TabIndex = 10;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.mygtukas0);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(96, 219);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(36, 36);
            this.button11.TabIndex = 11;
            this.button11.Text = ".";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.mygtukas0);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(138, 93);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(36, 36);
            this.button12.TabIndex = 12;
            this.button12.Text = "*";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Veiksmai_Clik);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(138, 135);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(36, 36);
            this.button13.TabIndex = 13;
            this.button13.Text = "-";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.Veiksmai_Clik);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(138, 178);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(36, 36);
            this.button14.TabIndex = 14;
            this.button14.Text = "+";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.Veiksmai_Clik);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(138, 219);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(36, 36);
            this.button15.TabIndex = 15;
            this.button15.Text = "=";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(180, 93);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 78);
            this.button1.TabIndex = 16;
            this.button1.Text = "/";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Veiksmai_Clik);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(180, 178);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 78);
            this.button2.TabIndex = 17;
            this.button2.Text = "CE";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tarpinis
            // 
            this.tarpinis.AutoSize = true;
            this.tarpinis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tarpinis.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tarpinis.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tarpinis.Location = new System.Drawing.Point(133, 65);
            this.tarpinis.Name = "tarpinis";
            this.tarpinis.Size = new System.Drawing.Size(0, 17);
            this.tarpinis.TabIndex = 18;
            this.tarpinis.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 261);
            this.Controls.Add(this.tarpinis);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.mygtukas8);
            this.Controls.Add(this.mygtukas4);
            this.Controls.Add(this.mygtukas5);
            this.Controls.Add(this.mygtukas1);
            this.Controls.Add(this.mygtukas2);
            this.Controls.Add(this.mygtukas3);
            this.Controls.Add(this.mygtukas6);
            this.Controls.Add(this.mygtukas9);
            this.Controls.Add(this.mygtukas7);
            this.Controls.Add(this.Ekranas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button mygtukas7;
        private System.Windows.Forms.Button mygtukas9;
        private System.Windows.Forms.Button mygtukas6;
        private System.Windows.Forms.Button mygtukas3;
        private System.Windows.Forms.Button mygtukas2;
        private System.Windows.Forms.Button mygtukas1;
        private System.Windows.Forms.Button mygtukas5;
        private System.Windows.Forms.Button mygtukas4;
        private System.Windows.Forms.Button mygtukas8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label tarpinis;
    }
}

