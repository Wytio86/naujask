﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace naujasTestas
{
    public partial class Form1 : Form
    {
        double verte = 0;
        string veiksmai = "";
        bool veiksmu_seka = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void mygtukas0(object sender, EventArgs e)
        {
            if ((Ekranas.Text == "0")||(veiksmu_seka))
                Ekranas.Clear();
            Button b = (Button)sender;
            Ekranas.Text =Ekranas.Text +b.Text;  
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Ekranas.Text = "0";
        }

        private void Veiksmai_Clik(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            veiksmai = b.Text;
            verte = double.Parse(Ekranas.Text);
            veiksmu_seka = true;
            tarpinis.Text = verte + " " + veiksmai;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            tarpinis.Text = "";
            switch (veiksmai)
            {
                case "+":
                    Ekranas.Text = (verte + double.Parse(Ekranas.Text)).ToString();
                    break;
                case "-":
                    Ekranas.Text = (verte - double.Parse(Ekranas.Text)).ToString();
                    break;
                case "*":
                    Ekranas.Text = (verte * double.Parse(Ekranas.Text)).ToString();
                    break;
                case "/":
                    Ekranas.Text = (verte / double.Parse(Ekranas.Text)).ToString();
                    break;
                default:
                    break;
                   
            }
            veiksmu_seka = false;
        }
    }
}
